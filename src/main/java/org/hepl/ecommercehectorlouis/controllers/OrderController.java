package org.hepl.ecommercehectorlouis.controllers;

import org.hepl.ecommercehectorlouis.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.Set;

@Controller
public class OrderController {
    @Autowired
    ArticleRepository articleRepository;

    @Autowired
    CartItemRepository cartItemRepository;

    @Autowired
    ClientRepository clientRepository;

    @Autowired
    CommandeRepository commandeRepository;

    @RequestMapping("/createorder")
    public String createOrder(HttpServletRequest request, Model model) {
        Client client = clientRepository.findClientByNom(request.getRemoteUser());
        Set<Commande> commandes = commandeRepository.findCommandesByClientAndPaiementEffectue(client, false);
        if(commandes.isEmpty()) {
            commandeRepository.save(new Commande(client));
            commandes = commandeRepository.findCommandesByClientAndPaiementEffectue(client, false);
        }

        Commande commande = commandes.iterator().next();
        BigDecimal total = new BigDecimal("0");
        if(commande.getCartItems() != null) {
            for (CartItem cartItem : commande.getCartItems()) {
                BigDecimal prix = new BigDecimal(cartItem.getQuantite());
                prix = prix.multiply(cartItem.getId().getArticle().getPrix());
                total = total.add(prix);
            }
        }

        model.addAttribute("id", commande.getId());
        model.addAttribute("statut", commande.getStatut());
        model.addAttribute("cartitems", commande.getCartItems());
        model.addAttribute("total", total);
        model.addAttribute("articles", articleRepository.findAll());
        return "store";
    }
}
