package org.hepl.ecommercehectorlouis.controllers;

import org.hepl.ecommercehectorlouis.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;

@Controller
public class TVAController {
    @Autowired
    ArticleRepository articleRepository;

    @Autowired
    CartItemRepository cartItemRepository;

    @Autowired
    ClientRepository clientRepository;

    @Autowired
    CommandeRepository commandeRepository;

    @GetMapping("/gettva")
    public String getTVA(HttpServletRequest request, @RequestParam(value = "id", defaultValue = "uninit") String id, Model model) {
        try {
            Article article = articleRepository.findArticleById(Long.parseLong(id));
            model.addAttribute("article", article.getNom());
            if("Livres".equals(article.getCategorie()))
                model.addAttribute("tva", "6%");
            else
                model.addAttribute("tva", "21%");
        }
        catch(NumberFormatException ex) {

        }
        return "gettva";
    }
}
