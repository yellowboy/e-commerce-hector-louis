package org.hepl.ecommercehectorlouis.controllers;

import org.hepl.ecommercehectorlouis.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Set;

@Controller
public class CheckoutController {
    @Autowired
    ArticleRepository articleRepository;

    @Autowired
    CartItemRepository cartItemRepository;

    @Autowired
    ClientRepository clientRepository;

    @Autowired
    CommandeRepository commandeRepository;

    @GetMapping("/docheckout")
    public String doCheckout(HttpServletRequest request, @RequestParam(value = "livraison", defaultValue = "normal") String livraison, Model model) {
        Client client = clientRepository.findClientByNom(request.getRemoteUser());
        Set<Commande> commandes = commandeRepository.findCommandesByClientAndPaiementEffectue(client, false);
        boolean valid = false;

        Commande commande = commandes.iterator().next();
        if(commande != null) {
            BigDecimal total = new BigDecimal("0");
            if(commande.getCartItems() != null) {
                for (CartItem cartItem : commande.getCartItems()) {
                    if (cartItem.getQuantite() <= 0 || cartItem.getQuantite() > cartItem.getId().getArticle().getQuantite()) {
                        valid = false;
                        break;
                    }
                    else {
                        valid = true;
                        cartItem.getId().getArticle().setQuantite(cartItem.getId().getArticle().getQuantite() - cartItem.getQuantite());
                    }
                    BigDecimal prix = cartItem.getId().getArticle().getPrix();
                    if ("Livres".equals(cartItem.getId().getArticle().getCategorie()))
                        prix = prix.add(prix.multiply(new BigDecimal("0.06")));
                    else
                        prix = prix.add(prix.multiply(new BigDecimal("0.21")));
                    prix = prix.multiply(new BigDecimal(cartItem.getQuantite()));
                    total = total.add(prix);
                }
                if(valid) {
                    if ("normal".equals(livraison))
                        total = total.add(new BigDecimal("5"));
                    else if ("express".equals(livraison))
                        total = total.add(new BigDecimal("10"));
                    total = total.setScale(2, RoundingMode.CEILING);
                    commande.setMontant(total);
                    if (total.compareTo(client.getMontant()) <= 0)
                        commande.setPaiementEffectue(true);
                    commandeRepository.save(commande);
                }
            }
        }
        model.addAttribute("commandes", commandeRepository.findCommandesByClientAndPaiementEffectue(client, true));
        return "orders";
    }
}
