package org.hepl.ecommercehectorlouis.controllers;

import org.hepl.ecommercehectorlouis.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.Set;

@Controller
public class CartController {
    @Autowired
    ArticleRepository articleRepository;

    @Autowired
    CartItemRepository cartItemRepository;

    @Autowired
    ClientRepository clientRepository;

    @Autowired
    CommandeRepository commandeRepository;

    @GetMapping("/additem")
    public String additem(HttpServletRequest request, @RequestParam(value = "id", defaultValue = "uninit") String id, @RequestParam(value = "quantite", defaultValue = "uninit") String quantite, Model model) {
        Client client = clientRepository.findClientByNom(request.getRemoteUser());
        Set<Commande> commandes = commandeRepository.findCommandesByClientAndPaiementEffectue(client, false);
        if(commandes.isEmpty()) {
            commandeRepository.save(new Commande(client));
            commandes = commandeRepository.findCommandesByClientAndPaiementEffectue(client, false);
        }

        Commande commande = commandes.iterator().next();
        try {
            Article article = articleRepository.findArticleById(Long.parseLong(id));
            CartItem cartItem = new CartItem(new CartItemIdentity(commande, article), Integer.parseInt(quantite));
            cartItemRepository.save(cartItem);
        }
        catch(NumberFormatException ex) {

        }
        BigDecimal total = new BigDecimal("0");
        for (CartItem cartItem : commande.getCartItems()) {
            BigDecimal prix = new BigDecimal(cartItem.getQuantite());
            prix = prix.multiply(cartItem.getId().getArticle().getPrix());
            total = total.add(prix);
        }

        model.addAttribute("id", commande.getId());
        model.addAttribute("statut", commande.getStatut());
        model.addAttribute("cartitems", commande.getCartItems());
        model.addAttribute("total", total);
        model.addAttribute("articles", articleRepository.findAll());
        return "store";
    }

    @GetMapping("/removeitem")
    public String removeitem(HttpServletRequest request, @RequestParam(value = "id", defaultValue = "uninit") String id, Model model) {
        Client client = clientRepository.findClientByNom(request.getRemoteUser());
        Set<Commande> commandes = commandeRepository.findCommandesByClientAndPaiementEffectue(client, false);
        if(commandes.isEmpty()) {
            commandeRepository.save(new Commande(client));
            commandes = commandeRepository.findCommandesByClientAndPaiementEffectue(client, false);
        }

        Commande commande = commandes.iterator().next();
        try {
            Article article = articleRepository.findArticleById(Long.parseLong(id));
            CartItem cartItem = cartItemRepository.findCartItemById(new CartItemIdentity(commande, article));
            cartItemRepository.delete(cartItem);
        }
        catch(NumberFormatException ex) {

        }
        BigDecimal total = new BigDecimal("0");
        if(commande.getCartItems() != null) {
            for (CartItem cartItem : commande.getCartItems()) {
                BigDecimal prix = new BigDecimal(cartItem.getQuantite());
                prix = prix.multiply(cartItem.getId().getArticle().getPrix());
                total = total.add(prix);
            }
        }

        model.addAttribute("id", commande.getId());
        model.addAttribute("statut", commande.getStatut());
        model.addAttribute("cartitems", commande.getCartItems());
        model.addAttribute("total", total);
        model.addAttribute("articles", articleRepository.findAll());
        return "store";
    }
}
