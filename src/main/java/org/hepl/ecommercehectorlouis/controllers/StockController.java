package org.hepl.ecommercehectorlouis.controllers;

import org.hepl.ecommercehectorlouis.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;

@Controller
public class StockController {
    @Autowired
    ArticleRepository articleRepository;

    @Autowired
    CartItemRepository cartItemRepository;

    @Autowired
    ClientRepository clientRepository;

    @Autowired
    CommandeRepository commandeRepository;

    @GetMapping("/checkinventory")
    public String checkInventory(HttpServletRequest request, @RequestParam(value = "id", defaultValue = "uninit") String id, @RequestParam(value = "quantite", defaultValue = "uninit") String quantite, Model model) {
        try {
            Article article = articleRepository.findArticleById(Long.parseLong(id));
            model.addAttribute("article", article.getNom());
            if(article.getQuantite() - Integer.parseInt(quantite) >= 0)
                model.addAttribute("quantite", "L'article est encore disponible en " + Integer.parseInt(quantite) + " exemplaires (total en stock : " + article.getQuantite() + ")");
            else
                model.addAttribute("quantite", "L'article n'est plus disponible en " + Integer.parseInt(quantite) + " exemplaires (total en stock : " + article.getQuantite() + ")");
        }
        catch(NumberFormatException ex) {

        }
        return "checkinventory";
    }
}
