package org.hepl.ecommercehectorlouis;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.math.BigDecimal;

@SpringBootApplication
public class ECommerceHectorLouisApplication {

    public static void main(String[] args) {
        SpringApplication.run(ECommerceHectorLouisApplication.class, args);
    }

    @Bean
    public CommandLineRunner demo(ClientRepository clientRepository,
                                  ArticleRepository articleRepository,
                                  CommandeRepository commandeRepository,
                                  CartItemRepository cartItemRepository) {
        return (args) -> {
            Client client = new Client("john", "Paper Street 11", new BigDecimal("555.55"));
            clientRepository.save(client);

            Article article = new Article("Bible", "Livres", new BigDecimal("9.99"), 10);
            articleRepository.save(article);
            articleRepository.save(new Article("Matrix", "Films", new BigDecimal("19.99"), 5));
        };
    }
}
