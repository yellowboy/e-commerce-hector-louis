package org.hepl.ecommercehectorlouis;

import org.springframework.data.repository.CrudRepository;

import java.util.Set;

public interface CommandeRepository extends CrudRepository<Commande, Long> {
    Set<Commande> findCommandesByClientAndPaiementEffectue(Client client, Boolean paiementEffectue);
}
