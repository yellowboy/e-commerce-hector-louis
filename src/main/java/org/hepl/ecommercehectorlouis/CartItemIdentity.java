package org.hepl.ecommercehectorlouis;

import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;
import java.io.Serializable;
import java.util.Objects;


@Embeddable
public class CartItemIdentity implements Serializable {
    @ManyToOne
    private Commande commande;

    @ManyToOne
    private Article article;

    protected CartItemIdentity() {

    }

    public CartItemIdentity(Commande commande, Article article) {
        this.commande = commande;
        this.article = article;
    }

    public Commande getCommande() {
        return commande;
    }

    public Article getArticle() {
        return article;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CartItemIdentity that = (CartItemIdentity) o;
        return Objects.equals(commande, that.commande) && Objects.equals(article, that.article);
    }

    @Override
    public int hashCode() {
        return Objects.hash(commande, article);
    }
}
