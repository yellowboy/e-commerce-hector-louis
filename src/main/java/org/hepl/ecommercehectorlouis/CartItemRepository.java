package org.hepl.ecommercehectorlouis;

import org.springframework.data.repository.CrudRepository;

public interface CartItemRepository extends CrudRepository<CartItem, CartItemIdentity> {
    CartItem findCartItemById(CartItemIdentity cartItemIdentity);
}
