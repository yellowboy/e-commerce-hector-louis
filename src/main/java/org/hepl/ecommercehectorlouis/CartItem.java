package org.hepl.ecommercehectorlouis;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class CartItem implements Serializable {
    @EmbeddedId
    private CartItemIdentity id;

    @Column(nullable = false)
    private Integer quantite;

    protected CartItem() {

    }

    public CartItem(CartItemIdentity id, Integer quantite) {
        this.id = id;
        this.quantite = quantite;
    }

    public CartItemIdentity getId() {
        return id;
    }

    public void setId(CartItemIdentity id) {
        this.id = id;
    }

    public Integer getQuantite() {
        return quantite;
    }

    public void setQuantite(Integer quantite) {
        this.quantite = quantite;
    }
}
