package org.hepl.ecommercehectorlouis.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class MvcConfig implements WebMvcConfigurer {

    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/notprotected").setViewName("notprotected");
        registry.addViewController("/").setViewName("notprotected");
        registry.addViewController("/protected").setViewName("protected");
        registry.addViewController("/store").setViewName("store");
        registry.addViewController("/orders").setViewName("orders");
        registry.addViewController("/checkinventory").setViewName("checkinventory");
        registry.addViewController("/gettva").setViewName("gettva");
        registry.addViewController("/login").setViewName("login");
        registry.addViewController("/logout").setViewName("disconnected");
    }

}
