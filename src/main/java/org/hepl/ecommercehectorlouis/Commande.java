package org.hepl.ecommercehectorlouis;

import com.sun.istack.NotNull;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Set;

@Entity
public class Commande {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @ManyToOne(optional = false)
    private Client client;

    @Column(nullable = false)
    private String statut;

    @Column(nullable = false)
    private BigDecimal montant;

    @Column(nullable = false)
    private Boolean paiementEffectue;

    @OneToMany(mappedBy = "id.commande")
    private Set<CartItem> cartItems;

    protected Commande() {

    }

    public Commande(Client client) {
        this.client = client;
        this.statut = "EN PREPARATION";
        this.montant = new BigDecimal("0");
        this.paiementEffectue = false;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public String getStatut() {
        return statut;
    }

    public void setStatut(String statut) {
        this.statut = statut;
    }

    public BigDecimal getMontant() {
        return montant;
    }

    public void setMontant(BigDecimal montant) {
        this.montant = montant;
    }

    public Boolean isPaiementEffectue() {
        return paiementEffectue;
    }

    public void setPaiementEffectue(Boolean paiementEffectue) {
        this.paiementEffectue = paiementEffectue;
    }

    public Set<CartItem> getCartItems() {
        return cartItems;
    }

    public void setCartItems(Set<CartItem> cartItems) {
        this.cartItems = cartItems;
    }
}
