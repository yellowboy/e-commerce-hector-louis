package org.hepl.ecommercehectorlouis;

import org.springframework.data.repository.CrudRepository;

public interface ArticleRepository extends CrudRepository<Article, Long> {
    Article findArticleById(Long id);
}
