package org.hepl.ecommercehectorlouis;

import org.springframework.data.repository.CrudRepository;

public interface ClientRepository  extends CrudRepository<Client, Long> {
    Client findClientByNom(String nom);
}
